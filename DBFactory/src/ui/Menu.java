package ui;

import java.sql.SQLException;
import model.DAL;
import model.Atleta;

import java.util.Scanner;

public class Menu {

    static Scanner sc = new Scanner(System.in);
    static int op;

    public static void Menu() throws SQLException {
        System.out.println("Selecione opção: ");
        System.out.println("1: Base de Dados");
        System.out.println("2: Dados de Tabelas");
        System.out.println("0: Quit");
        op = sc.nextInt();
        do {
            switch (op) {
                case 1:
                    System.out.println("1: Adicionar Base de Dados");
                    System.out.println("2: Mostrar Base de Dados");
                    System.out.println("3: Alterar Base de Dados");
                    System.out.println("4: Remover Base de Dados");
                    System.out.println("0: Quit");
                    op = sc.nextInt();
                    do {
                        switch (op) {
                            case 1:
                                break;
                            case 2:
                                break;
                            case 3:
                                break;
                            case 4:
                                break;
                        }
                    } while (op != 0);

                    break;
                case 2:
                    System.out.println("Escolha a tabela: ");
                    System.out.println("1: Secção");
                    System.out.println("2: Modalidade");
                    System.out.println("3: TipoAtleta");
                    System.out.println("4: Atleta");
                    System.out.println("5: Convocatórioa");
                    System.out.println("6: ConvocatóriaAtleta");
                    System.out.println("7: Prova");
                    System.out.println("8: AtletaProva");
                    op = sc.nextInt();
                    do {
                        switch (op) {
                            case 1:
                                System.out.println("1: Adicionar Secção");
                                System.out.println("2: Mostrar Secção");
                                System.out.println("3: Alterar Secção");
                                System.out.println("4: Remover Secção");
                                System.out.println("0: Quit");
                                op = sc.nextInt();
                                break;
                            case 2:
                                System.out.println("1: Adicionar Modalidade");

                                System.out.println("2: Mostrar Modalidade");
                                System.out.println("3: Alterar Modalidade");
                                System.out.println("4: Remover Modalidade");
                                System.out.println("0: Quit");
                                op = sc.nextInt();
                                break;
                            case 3:
                                System.out.println("1: Adicionar TipoAtleta");
                                System.out.println("2: Mostrar TipoAtleta");
                                System.out.println("3: Alterar TipoAtleta");
                                System.out.println("4: Remover TipoAtleta");
                                System.out.println("0: Quit");
                                op = sc.nextInt();
                                break;
                            case 4:
                                System.out.println("1: Adicionar Atleta");
                                System.out.println("2: Mostrar Atletas");
                                Atleta atl = new Atleta();
                                DAL.read(2, atl);
                                System.out.println(atl.getNome());
                                System.out.println("3: Alterar Atleta");
                                System.out.println("4: Remover Atleta");
                                System.out.println("0: Quit");
                                op = sc.nextInt();
                                break;
                            case 5:
                                System.out.println("1: Adicionar Convocatória");
                                System.out.println("2: Mostrar Convocatória");
                                System.out.println("3: Alterar Convocatória");
                                System.out.println("4: Remover Convocatória");
                                System.out.println("0: Quit");
                                op = sc.nextInt();
                                break;
                            case 6:
                                System.out.println("1: Adicionar ConvocatóriaAtleta");
                                System.out.println("2: Mostrar ConvocatóriaAtleta");
                                System.out.println("3: Alterar ConvocatóriaAtleta");
                                System.out.println("4: Remover ConvocatóriaAtleta");
                                System.out.println("0: Quit");
                                op = sc.nextInt();
                                break;
                            case 7:
                                System.out.println("1: Adicionar Prova");
                                System.out.println("2: Mostrar Prova");
                                System.out.println("3: Alterar Prova");
                                System.out.println("4: Remover Prova");
                                System.out.println("0: Quit");
                                op = sc.nextInt();
                                break;
                            case 8:
                                System.out.println("1: Adicionar AtletaProva");
                                System.out.println("2: Mostrar AtletaProva");
                                System.out.println("3: Alterar AtletaProva");
                                System.out.println("4: Remover AtletaProva");
                                System.out.println("0: Quit");
                                op = sc.nextInt();
                                break;
                        }
                    } while (op != 0);
                    break;
            }
        } while (op != 0);
    }
}
