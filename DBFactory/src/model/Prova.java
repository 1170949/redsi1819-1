/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import Data.Data;

/**
 *
 * @author andre
 */
public class Prova {
    private String nome;
    private String local;
    private Data data_prova;

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * @return the data_prova
     */
    public Data getData_prova() {
        return data_prova;
    }

    /**
     * @param data_prova the data_prova to set
     */
    public void setData_prova(Data data_prova) {
        this.data_prova = data_prova;
    }
}
