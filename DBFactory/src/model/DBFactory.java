package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBFactory {

    static final String URL = "jdbc:mysql://localhost:3306/t1";
    static final String USER = "root";
    static final String PASS = "bruno.sousa1";

    public static Connection getConnection() {
        try {
            Connection conn = DriverManager.getConnection(URL, USER, PASS);
            System.out.println(String.format("Connection to %s succeeded!", conn.getCatalog()));

            return conn;
        } catch (SQLException exc) {
            throw new RuntimeException("!!ERROR CONNECTING!!", exc);
        }
    }

}
