package model;

public class Atleta {
    private String nome;
    private int idatleta;
    private int idtipoatleta;
    private int idade;
    private String nacionalidade;
    private String tipo;
    private boolean retirado;

    public Atleta(String nome, int idatleta, int idtipoatleta, int idade, String nacionalidade, String tipo, boolean retirado) {
        this.nome = nome;
        this.idatleta = idatleta;
        this.idtipoatleta = idtipoatleta;
        this.idade = idade;
        this.nacionalidade = nacionalidade;
        this.tipo = tipo;
        this.retirado = retirado;
    }

    

    public Atleta() {
        
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the idade
     */
    public int getIdade() {
        return idade;
    }

    /**
     * @param idade the idade to set
     */
    public void setIdade(int idade) {
        this.idade = idade;
    }

    /**
     * @return the nacionalidade
     */
    public String getNacionalidade() {
        return nacionalidade;
    }

    /**
     * @param nacionalidade the nacionalidade to set
     */
    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the retirado
     */
    public boolean isRetirado() {
        return retirado;
    }

    /**
     * @param retirado the retirado to set
     */
    public void setRetirado(boolean retirado) {
        this.retirado = retirado;
    }

    /**
     * @return the idatleta
     */
    public int getIdatleta() {
        return idatleta;
    }

    /**
     * @param idatleta the idatleta to set
     */
    public void setIdatleta(int idatleta) {
        this.idatleta = idatleta;
    }

    /**
     * @return the idtipoatleta
     */
    public int getIdtipoatleta() {
        return idtipoatleta;
    }

    /**
     * @param idtipoatleta the idtipoatleta to set
     */
    public void setIdtipoatleta(int idtipoatleta) {
        this.idtipoatleta = idtipoatleta;
    }
}
