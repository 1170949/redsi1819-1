/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Data.Data;

/**
 *
 * @author andre
 */
public class Modalidade {
    private String Designacao;
    private Data data_criacao;

    /**
     * @return the Designacao
     */
    public String getDesignacao() {
        return Designacao;
    }

    /**
     * @param Designacao the Designacao to set
     */
    public void setDesignacao(String Designacao) {
        this.Designacao = Designacao;
    }

    /**
     * @return the data_criacao
     */
    public Data getData_criacao() {
        return data_criacao;
    }

    /**
     * @param data_criacao the data_criacao to set
     */
    public void setData_criacao(Data data_criacao) {
        this.data_criacao = data_criacao;
    }
}
