/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.redsi.clube.model;

/**
 *
 * @author andre
 */
public class Convocatoria {

    private int idConvocatoria;
    private String designacao;
    private Modalidade idModalidade;

    public Convocatoria() {
    }

    
    
    public Convocatoria(int idConvocatoria, String designacao, Modalidade idModalidade) {
        this.idConvocatoria = idConvocatoria;
        this.designacao = designacao;
        this.idModalidade = idModalidade;
    }

    /**
     * @return the idConvocatoria
     */
    public int getIdConvocatoria() {
        return idConvocatoria;
    }

    /**
     * @param idConvocatoria the idConvocatoria to set
     */
    public void setIdConvocatoria(int idConvocatoria) {
        this.idConvocatoria = idConvocatoria;
    }

    /**
     * @return the designacao
     */
    public String getDesignacao() {
        return designacao;
    }

    /**
     * @param designacao the designacao to set
     */
    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    /**
     * @return the idModalidade
     */
    public Modalidade getIdModalidade() {
        return idModalidade;
    }

    /**
     * @param idModalidade the idModalidade to set
     */
    public void setIdModalidade(Modalidade idModalidade) {
        this.idModalidade = idModalidade;
    }

    

}
