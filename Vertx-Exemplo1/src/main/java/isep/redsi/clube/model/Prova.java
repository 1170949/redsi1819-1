/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.redsi.clube.model;

import isep.redsi.clube.data.Data;

/**
 *
 * @author andre
 */
public class Prova {
    private Modalidade modalidade;
    private int idProva;
    private String nome;
    private String local;
    private Data data_prova;

    public Prova(Modalidade modalidade, int idProva, String nome, String local, Data data_prova) {
        this.modalidade = modalidade;
        this.idProva = idProva;
        this.nome = nome;
        this.local = local;
        this.data_prova = data_prova;
    }

    

    /**
     * @return the modalidade
     */
    public Modalidade getModalidade() {
        return modalidade;
    }

    /**
     * @param modalidade the modalidade to set
     */
    public void setModalidade(Modalidade modalidade) {
        this.modalidade = modalidade;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * @return the data_prova
     */
    public Data getData_prova() {
        return data_prova;
    }

    /**
     * @param data_prova the data_prova to set
     */
    public void setData_prova(Data data_prova) {
        this.data_prova = data_prova;
    }

    /**
     * @return the idProva
     */
    public int getIdProva() {
        return idProva;
    }

    /**
     * @param idProva the idProva to set
     */
    public void setIdProva(int idProva) {
        this.idProva = idProva;
    }
    
}
