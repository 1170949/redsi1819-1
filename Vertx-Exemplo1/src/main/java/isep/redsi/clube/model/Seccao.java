/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.redsi.clube.model;

import isep.redsi.clube.data.Data;

/**
 *
 * @author andre
 */
public class Seccao {

    private int id_seccao;
    private Data data_criacao;
    private String designacao;
    private Modalidade modalidade;

    public Seccao(int id_seccao, Data data_criacao, String designacao, Modalidade modalidade) {
        this.id_seccao = id_seccao;
        this.data_criacao = data_criacao;
        this.designacao = designacao;
        this.modalidade = modalidade;
    }

    

    /**
     * @return the id_seccao
     */
    public int getId_seccao() {
        return id_seccao;
    }

    /**
     * @param id_seccao the id_seccao to set
     */
    public void setId_seccao(int id_seccao) {
        this.id_seccao = id_seccao;
    }

    /**
     * @return the data_cricao
     */
    public Data getData_criacao() {
        return data_criacao;
    }

    /**
     * @param data_cricao the data_cricao to set
     */
    public void setData_criacao(Data data_criacao) {
        this.data_criacao = data_criacao;
    }

    /**
     * @return the designacao
     */
    public String getDesignacao() {
        return designacao;
    }

    /**
     * @param designacao the designacao to set
     */
    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    /**
     * @return the modalidade
     */
    public Modalidade getModalidade() {
        return modalidade;
    }

    /**
     * @param modalidade the modalidade to set
     */
    public void setModalidade(Modalidade modalidade) {
        this.modalidade = modalidade;
    }
}
