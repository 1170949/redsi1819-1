/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.redsi.clube.model;

import isep.redsi.clube.data.Data;

/**
 *
 * @author andre
 */
public class Modalidade {

    private String designacao;
    private Data data_criacao;
    private int idModalidade;
    private int idSeccao;

    public Modalidade(String designacao, Data data_criacao, int idModalidade, int idSeccao) {
        this.designacao = designacao;
        this.data_criacao = data_criacao;
        this.idModalidade = idModalidade;
        this.idSeccao = idSeccao;
    }

    

    /**
     * @return the designacao
     */
    public String getDesignacao() {
        return designacao;
    }

    /**
     * @param designacao the designacao to set
     */
    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    /**
     * @return the data_criacao
     */
    public Data getData_criacao() {
        return data_criacao;
    }

    /**
     * @param data_criacao the data_criacao to set
     */
    public void setData_criacao(Data data_criacao) {
        this.data_criacao = data_criacao;
    }

    /**
     * @return the idModalidade
     */
    public int getIdModalidade() {
        return idModalidade;
    }

    /**
     * @param idModalidade the idModalidade to set
     */
    public void setIdModalidade(int idModalidade) {
        this.idModalidade = idModalidade;
    }

    /**
     * @return the idSeccao
     */
    public int getIdSeccao() {
        return idSeccao;
    }

    /**
     * @param idSeccao the idSeccao to set
     */
    public void setIdSeccao(int idSeccao) {
        this.idSeccao = idSeccao;
    }

    
}
