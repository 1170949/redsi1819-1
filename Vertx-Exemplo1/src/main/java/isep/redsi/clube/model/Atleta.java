
package isep.redsi.clube.model;

/**
 *
 * @author albertosampaio
 */
public class Atleta {
    private int numero;
    private int idade;
    private String nome;
    private String nacionalidade;
    private int retirado;
    private String genero;
    private int idtipoatleta;

    public Atleta(int numero, int idade, String nome, String nacionalidade, int retirado, String genero, int idtipoatleta) {
        this.numero = numero;
        this.idade = idade;
        this.nome = nome;
        this.nacionalidade = nacionalidade;
        this.retirado = retirado;
        this.genero = genero;
        this.idtipoatleta = idtipoatleta;
    }
    
    
    // construtor sem parâmteros e setd necessários para Json recriar objecto
    public Atleta(){
    }
    public void setNumero(int numero){
        this.numero=numero;
    }
    public void setNome(String nome){
        this.nome=nome;
    }
    public Atleta(int numero, String nome){
        this.numero = numero;
        this.nome = nome;
    }
    public String getNome()
    {
        return nome;
    }

    /**
     * @return the numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * @return the idade
     */
    public int getIdade() {
        return idade;
    }

    /**
     * @param idade the idade to set
     */
    public void setIdade(int idade) {
        this.idade = idade;
    }

    /**
     * @return the nacionalidade
     */
    public String getNacionalidade() {
        return nacionalidade;
    }

    /**
     * @param nacionalidade the nacionalidade to set
     */
    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    

    /**
     * @return the retirado
     */
    public int getRetirado() {
        return retirado;
    }

    /**
     * @param retirado the retirado to set
     */
    public void setRetirado(int retirado) {
        this.retirado = retirado;
    }

    /**
     * @return the genero
     */
    public String getGenero() {
        return genero;
    }

    /**
     * @param genero the genero to set
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
     * @return the idtipoatleta
     */
    public int getIdtipoatleta() {
        return idtipoatleta;
    }

    /**
     * @param idtipoatleta the idtipoatleta to set
     */
    public void setIdtipoatleta(int idtipoatleta) {
        this.idtipoatleta = idtipoatleta;
    }
    
}
