/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.redsi.clube.servidor;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.EncodeException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import isep.redsi.clube.controller.AtletaController;
import isep.redsi.clube.controller.ConvocatoriaController;
import isep.redsi.clube.model.Atleta;
import isep.redsi.clube.model.Convocatoria;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author REDSI, ISEP
 */
public class WebServer extends AbstractVerticle {

    @Override
    public void start(Future<Void> fut) throws IOException {

        // Criar objecto router, 
        // Encaminhara´ os pedidos, de acordo com caminho no URL
        Router router = Router.router(vertx);

        /* SAO APENAS EXEMPLOS: */
        // Associa uma mmsg HTML a caminho "/lixo" (exemplo, URL: localhost:8080/lixo)
        router.route("/").handler(StaticHandler.create("webroot/index.html"));
        // Serve um recurso estático se acede raiz do site, com * para associar ficheiro CSS
        router.route("/*").handler(StaticHandler.create("webroot/index.html")); //usa index.html
        router.route("/atletas/*").handler(StaticHandler.create("webroot/atletas.html")); //usa index2.html
        router.route("/prova/*").handler(StaticHandler.create("webroot/prova.html"));
        router.route("/contacts/*").handler(StaticHandler.create("webroot/contact.html"));
        router.route("/convocatoria/*").handler(StaticHandler.create("webroot/convocatoria.html"));
//        router.get("/atletas/all").handler(this::getAll);
        router.get("/atletas/:num").handler(this::getAtleta);
        router.get("/atletas/:num").handler(this::getConvocatoria);
//        router.get("/atletas/:num").handler(this::getModalidade);
//        router.get("/atletas/:num").handler(this::getProva);
//        router.get("/atletas/:num").handler(this::getSeccao);//ainda sem página HTML

//        add a handler which sets the request body on the RoutingContext.
        router.post("/atletas/add*").handler(BodyHandler.create());
        router.post("/atletas/add").handler(this::addAtleta);
        router.delete("/atletas/:num").handler(this::deleteAtleta);
//        router.put("/atletas/:id").handler(this::updateOne);

        // Cria o servidor HTTP e passa o método accept() ao handler do pedido (request handler).
        vertx.createHttpServer().requestHandler(router::accept).listen(8080, (AsyncResult<HttpServer> result) -> {
            if (result.succeeded()) {
                fut.complete(); //concretiza criacao
            } else {
                fut.fail(result.cause()); // nao conseguiu criar o servidor
                System.err.println("Couldn't connect...");
            }
        });
    }

    // ainda sem exemplo no lado do cliente (HTML)
    private void getAtleta(RoutingContext routingContext) {
        System.out.println("showAtleta() - " + routingContext.toString());
        String numero = routingContext.request().getParam("num");
        System.out.println("numero: " + numero);
        try {
            int num = Integer.valueOf(numero);

            // E obtem atleta com esse num
            Atleta atleta = new Atleta();
            AtletaController.mostrarAtleta(num, atleta);
            System.out.println("atleta obtido: " + atleta.getNome());
            // Se atleta nao existe 
            //    routingContext.response().setStatusCode(404).end(); // recurso não encontrado
            // senao 
            routingContext.response()
                    .setStatusCode(200) // ok e devolve entidade solicitada
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(atleta));
        } catch (NumberFormatException e) {
            routingContext.response().setStatusCode(400).end();
        } catch (SQLException ex) {
            Logger.getLogger(WebServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void deleteAtleta(RoutingContext routingContext) {
        System.out.println("deleteAtleta() - " + routingContext.toString());
        String numero = routingContext.request().getParam("num");
        System.out.println("numero: " + numero);        
        try {
            int num = Integer.valueOf(numero);

            // E obtem atleta com esse num
            Atleta atleta = new Atleta();
            AtletaController.apagarAtleta(num, atleta);
            System.out.println("atleta apagado: " + atleta.getNome());
            // Se atleta nao existe 
            //    routingContext.response().setStatusCode(404).end(); // recurso não encontrado
            // senao 
            routingContext.response()
                    .setStatusCode(200) // ok e devolve entidade solicitada
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(atleta));
        } catch (NumberFormatException e) {
            routingContext.response().setStatusCode(400).end();
        } catch (SQLException ex) {
            Logger.getLogger(WebServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // com exemplo no lado do cliente (HTML)
    private void addAtleta(RoutingContext routingContext) {
        System.out.println("addAtleta() - " + routingContext.toString());
        try {
            JsonObject body = routingContext.getBodyAsJson();

            Atleta atleta = body.mapTo(Atleta.class);
            // aqui adicionar atleta
            System.out.println("atleta " + atleta.getNome() + " adicionado!");
            AtletaController.guardarAtleta(atleta);
            // responde com o novo atleta
            routingContext.response()
                    .setStatusCode(201) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(atleta));
        } catch (Exception e) {
            System.out.println("exception: " + e.getMessage());

            routingContext.response()
                    .setStatusCode(500) // ok e recurso criado
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodeToBuffer("{erro: 'erro!'}"));
        }
    }
    
    
    private void getConvocatoria(RoutingContext routingContext) {
        System.out.println("showConvocatoria() - " + routingContext.toString());
        String numero = routingContext.request().getParam("num");
        System.out.println("numero: " + numero);
        try {
            int num = Integer.valueOf(numero);

            // E obtem atleta com esse num
            Convocatoria convocatoria = new Convocatoria();
            ConvocatoriaController.showConvocatoria(num, convocatoria);
            System.out.println("Convocatória obtida: " + convocatoria.getDesignacao());
            // Se atleta nao existe 
            //    routingContext.response().setStatusCode(404).end(); // recurso não encontrado
            // senao 
            routingContext.response()
                    .setStatusCode(200) // ok e devolve entidade solicitada
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(convocatoria));
        } catch (NumberFormatException e) {
            routingContext.response().setStatusCode(400).end();
        } catch (SQLException ex) {
            Logger.getLogger(WebServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void getProva(RoutingContext routingContext) {
        System.out.println("showConvocatoria() - " + routingContext.toString());
        String numero = routingContext.request().getParam("num");
        System.out.println("numero: " + numero);
        try {
            int num = Integer.valueOf(numero);

            // E obtem atleta com esse num
            Convocatoria convocatoria = new Convocatoria();
            ConvocatoriaController.showConvocatoria(num, convocatoria);
            System.out.println("Convocatória obtida: " + convocatoria.getDesignacao());
            // Se atleta nao existe 
            //    routingContext.response().setStatusCode(404).end(); // recurso não encontrado
            // senao 
            routingContext.response()
                    .setStatusCode(200) // ok e devolve entidade solicitada
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end(Json.encodePrettily(convocatoria));
        } catch (NumberFormatException e) {
            routingContext.response().setStatusCode(400).end();
        } catch (SQLException ex) {
            Logger.getLogger(WebServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
