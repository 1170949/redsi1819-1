/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.redsi.clube.servidor;

import io.vertx.core.Vertx;
import java.util.concurrent.ExecutionException;

/**
 *
 * @author albertosampaio
 */
public class Main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

       // verificar se existe ligacao 'a base de dados
       // ...
       // cria servidor
       Vertx vertx = Vertx.vertx();
       vertx.deployVerticle(new WebServer());
    }
}
