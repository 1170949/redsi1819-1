/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.redsi.clube.controller;

import isep.redsi.clube.dal.DAL;
import isep.redsi.clube.model.Atleta;
import java.sql.SQLException;

/**
 *
 * @author andre
 */
public class AtletaController {
 
    
    public static void guardarAtleta(Atleta a) throws SQLException{
    DAL.addAtleta(a);
    }
    
    public static void mostrarAtleta (int num, Atleta a) throws SQLException{
    DAL.showAtleta(num, a);
    }
    
    public static void apagarAtleta(int num, Atleta a) throws SQLException{
    DAL.deleteAtleta(num,a);
    }
   
    
}
