/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.redsi.clube.controller;

import isep.redsi.clube.dal.DAL;
import isep.redsi.clube.model.Convocatoria;
import java.sql.SQLException;

/**
 *
 * @author andre
 */
public class ConvocatoriaController {
    public static void addConvocatoria (Convocatoria c) throws SQLException{
    DAL.addConvocatoria(c);
    }
    
    public static void showConvocatoria (int num,Convocatoria c) throws SQLException {
    DAL.showConvocatoria(num, c);
    }
}
