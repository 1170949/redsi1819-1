/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.redsi.clube.controller;

import isep.redsi.clube.dal.DAL;
import isep.redsi.clube.model.Modalidade;
import java.sql.SQLException;

/**
 *
 * @author andre
 */
public class ModalidadeController {
    public static void addConvocatoria (Modalidade m) throws SQLException{
    DAL.addModalidade(m);
    }
    
    public static void showConvocatoria (int num,Modalidade m) throws SQLException {
    DAL.showModalidade(num, m);
    }
}
