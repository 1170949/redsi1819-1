package isep.redsi.clube.dal;

import isep.redsi.clube.model.Atleta;
import isep.redsi.clube.model.Modalidade;
import isep.redsi.clube.model.Prova;
import isep.redsi.clube.model.Seccao;
import isep.redsi.clube.model.Convocatoria;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DAL {

    public static void showAtleta(int numero, Atleta a) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select nome from atleta where idatleta=" + numero + ";");
        try {
            rs.next();
            String aux = rs.getString("nome");
            a.setNome(aux);
            a.setNumero(numero);
        } catch (SQLException ex) {
            System.out.println("erro get: " + ex.getStackTrace());
        }
        conn.close();
    }
    
    

    public static boolean deleteAtleta(int numero, Atleta a) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        boolean rs=false;
        try {
            rs = stmt.execute("delete from atleta where idatleta="+numero+";");
        

        } catch (SQLException ex) {
            System.out.println("erro get: " + ex.getStackTrace());
        }
        conn.close();
        return rs;
    }

    public static boolean addAtleta(Atleta a) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        boolean res = stmt.execute("insert into atleta(idatleta,nome,idade,nacionalidade,retirado,genero,idtipoatleta) values(" + a.getNumero() + ",'" + a.getNome() + "'," + a.getIdade() + ",'"
                + a.getNacionalidade() + "'," + a.getRetirado() + ",'" + a.getGenero() + "'," + a.getIdtipoatleta() + ");");
        //execute este comando no mysql
        conn.close();
        return res;
    }

    public static void showConvocatoria(int numero, Convocatoria c) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select from convocatoria where idconvocatoria=" + numero + ";");
        try {
            rs.next();
            String aux = rs.getString("designacao");
            c.setDesignacao(aux);
            c.setIdConvocatoria(numero);
            c.setIdModalidade(c.getIdModalidade());
        } catch (SQLException ex) {
            System.out.println("erro get: " + ex.getStackTrace());
        }
        conn.close();
    }

    public static boolean addConvocatoria(Convocatoria c) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        boolean res = stmt.execute("insert into convocatoria(idconvocatoria,designacao,idmodalidade) values("
                + c.getIdConvocatoria() + ",'" + c.getDesignacao() + "'," + c.getIdModalidade() + "));");
        //execute este comando no mysql
        conn.close();
        return res;
    }

    //modalidade
    public static void showModalidade(int numero, Modalidade m) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("select nome from modalidade where idmodalidade=" + numero + ";");
        try {
            rs.next();
            String aux = rs.getString("designacao");
            m.setDesignacao(aux);
            m.setIdModalidade(numero);
            m.setIdSeccao(numero);
        } catch (SQLException ex) {
            System.out.println("erro get: " + ex.getStackTrace());
        }
        conn.close();
    }

    public static boolean addModalidade(Modalidade m) throws SQLException {
        Connection conn = DBFactory.getConnection();
        Statement stmt = conn.createStatement();
        boolean res = stmt.execute("insert into modalidade(designacao,data_criacao,idmodalidade,idseccao) values('"
                + m.getDesignacao() + "'," + m.getData_criacao() + "," + m.getIdModalidade() + "," + m.getIdSeccao() + ");");
        //execute este comando no mysql
        conn.close();
        return res;
    }
//prova

//    public static void showProva(int numero, Prova p) throws SQLException {
//        Connection conn = DBFactory.getConnection();
//        Statement stmt = conn.createStatement();
//        ResultSet rs = stmt.executeQuery("select nome from atleta where idatleta=" + numero + ";");
//        try {
//            rs.next();
//            String aux = rs.getString("nome");
//            p.setNome(aux);
//            p.setNumero(numero);
//        } catch (SQLException ex) {
//            System.out.println("erro get: " + ex.getStackTrace());
//        }
//        conn.close();
//    }
//
//    public static boolean addProva(Prova p) throws SQLException {
//        Connection conn = DBFactory.getConnection();
//        Statement stmt = conn.createStatement();
//        boolean res = stmt.execute("insert into prova(idprova,nome,local,data) values(" + p.getIdProva() + ",'"
//                + p.getNome() + "','" + p.getLocal() + "'," + p.getData_prova() + ");");
//        //execute este comando no mysql
//        conn.close();
//        return res;
//    }
////seccao
//
//    public static void showSeccao(int numero, Seccao s) throws SQLException {
//        Connection conn = DBFactory.getConnection();
//        Statement stmt = conn.createStatement();
//        ResultSet rs = stmt.executeQuery("select nome from atleta where idatleta=" + numero + ";");
//        try {
//            rs.next();
//            String aux = rs.getString("nome");
//            a.setNome(aux);
//            a.setNumero(numero);
//        } catch (SQLException ex) {
//            System.out.println("erro get: " + ex.getStackTrace());
//        }
//        conn.close();
//    }
//
//    public static boolean addSeccao(Seccao s) throws SQLException {
//        Connection conn = DBFactory.getConnection();
//        Statement stmt = conn.createStatement();
//        boolean res = stmt.execute("insert into seccao(idseccao,designacao,modalidade,data_criacao) values("
//                + s.getId_seccao() + ",'" + s.getDesignacao() + "','" + s.getModalidade() + "'," + s.getData_criacao() + ");");
//        //execute este comando no mysql
//        conn.close();
//        return res;
//    }
}
